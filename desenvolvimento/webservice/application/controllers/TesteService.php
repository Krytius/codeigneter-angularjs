<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TesteService extends MY_ControllerService {

    public function get() {
        $this->autentication();
        
        $i = array("Teste" => 1);
        $this->responseJSON($i);
    }

}
