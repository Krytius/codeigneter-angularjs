<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Controller de webservice
 */
class MY_ControllerService extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * Autenticação para uso da api
     */
    protected function autentication() {
        if (isset($_SERVER["PHP_AUTH_USER"]) && isset($_SERVER["PHP_AUTH_USER"])) {
            $user = $_SERVER["PHP_AUTH_USER"];
            $senha = $_SERVER["PHP_AUTH_PW"];    
            
            if ($this->validacao($user, $senha)) {
                return;
            }
        }
        
        $this->responseJSON(array("error" => true));
        exit;
    }

    /**
     * Validação do usuário e senha para o Basic auth
     * @param type $user
     * @param type $senha
     * @return boolean
     */
    private function validacao($user, $senha) {
        return true;
    }

    /**
     * Retorno em json para cliente
     * @param type $obj
     */
    protected function responseJSON($obj) {
        header('Content-Type: application/json');
        echo json_encode($obj);
    }

}
