<?php

/**
 * ------------- DO NOT UPLOAD THIS FILE TO LIVE SERVER ---------------------
 *
 * Implements code completion for CodeIgniter in phpStorm
 * phpStorm indexes all class constructs, so if this file is in the project it will be loaded.
 *
 *
 * This property values were borrowed from another site on the internet
 * This is just a better way to implement it, rather than editing core CI files
 *
 * PHP version 5
 *
 * LICENSE: GPL http://www.gnu.org/copyleft/gpl.html
 *
 * Created 1/28/12, 11:06 PM
 *
 * @category
 * @package    CodeIgniter CI_phpStorm.php
 * @author     Jeff Behnke
 * @copyright  2009-11 Valid-Webs.com
 * @license    GPL http://www.gnu.org/copyleft/gpl.html
 * @version    2012.02.03
 */

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_active_record $dbAcao
 * @property CI_DB_active_record $dbConsulta
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 * @property CI_Lang $lang                        Language Class
 * @property CI_Javascript $javascript            Javascript Class
 * @property CI_Jquery $jquery                    Jquery Class
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 * @property CI_Security $security                Security Class, xss, csrf, etc...
 * @property CI_Driver_Library $driver            CodeIgniter Driver Library Class
 * @property CI_Cache $cache                      CodeIgniter Caching Class
 *
 * // Model
 * @property agenda_model $agenda_model
 * @property bairro_model $bairro_model
 * @property cidade_model $cidade_model
 * @property configuracao_model $configuracao_model
 * @property contatos_model $contatos_model
 * @property convidados_model $convidados_model
 * @property dadosprofissionais_model $dadosprofissionais_model
 * @property disponibilidade_model $disponibilidade_model
 * @property especialidade_dadosprofissionais_model $especialidade_dadosprofissionais_model
 * @property especialidade_model $especialidade_model
 * @property hospital_model $hospital_model
 * @property localatendimento_model $localatendimento_model
 * @property localevento_model $localevento_model
 * @property matricula_model $matricula_model
 * @property menuexcecao_model $menuexcecao_model
 * @property menu_model $menu_model
 * @property pacote_model $pacote_model
 * @property perfil_model $perfil_model
 * @property pertencepacoteexcecao_model $pertencepacoteexcecao_model
 * @property procedimentoexcecao_model $procedimentoexcecao_model
 * @property procedimento_model $procedimento_model
 * @property produto_model $produto_model
 * @property seguimento_model $seguimento_model
 * @property seguimento_dadosprofissionais_model $seguimento_dadosprofissionais_model
 * @property senha_model $senha_model
 * @property tipo_dadosprofissionais_model $tipo_dadosprofissionais_model
 * @property tipo_model $tipo_model
 * @property usuario_model $usuario_model
 *
 * //  Library
 * @property definelayout $definelayout
 * @property validacao $validacao
 * @property emailsistema $emailsistema
 * @property convertperfil $convertperfil
 * @property my_elephant $my_elephant
 *
 */
class CI_Controller
{
}

;

/**
 * @property CI_DB_active_record $db
 * @property CI_DB_active_record $dbAcao
 * @property CI_DB_active_record $dbConsulta
 * @property CI_DB_forge $dbforge
 * @property CI_Config $config
 * @property CI_Loader $load
 * @property CI_Session $session
 * @property CI_DB_result
 * @property CI_Benchmark $benchmark              This class enables you to mark points and calculate the time difference between them.<br />  Memory consumption can also be displayed.
 * @property CI_Calendar $calendar                This class enables the creation of calendars
 * @property CI_Cart $cart                        Shopping Cart Class
 * @property CI_Controller $controller            This class object is the super class that every library in.<br />CodeIgniter will be assigned to.
 * @property CI_Email $email                      Permits email to be sent using Mail, Sendmail, or SMTP.
 * @property CI_Encrypt $encrypt                  Provides two-way keyed encoding using XOR Hashing and Mcrypt
 * @property CI_Exceptions $exceptions            Exceptions Class
 * @property CI_Form_validation $form_validation  Form Validation Class
 * @property CI_Ftp $ftp                          FTP Class
 * @property CI_Hooks $hooks                      //dead
 * @property CI_Image_lib $image_lib              Image Manipulation class
 * @property CI_Input $input                      Pre-processes global input data for security
 * @property CI_Lang $lang                        Language Class
 * @property CI_Log $log                          Logging Class
 * @property CI_Model $model                      CodeIgniter Model Class
 * @property CI_Output $output                    Responsible for sending final output to browser
 * @property CI_Pagination $pagination            Pagination Class
 * @property CI_Parser $parser                    Parses pseudo-variables contained in the specified template view,<br />replacing them with the data in the second param
 * @property CI_Profiler $profiler                This class enables you to display benchmark, query, and other data<br />in order to help with debugging and optimization.
 * @property CI_Router $router                    Parses URIs and determines routing
 * @property CI_Sha1 $sha1                        Provides 160 bit hashing using The Secure Hash Algorithm
 * @property CI_Table $table                      HTML table generation<br />Lets you create tables manually or from database result objects, or arrays.
 * @property CI_Trackback $trackback              Trackback Sending/Receiving Class
 * @property CI_Typography $typography            Typography Class
 * @property CI_Unit_test $unit_test              Simple testing class
 * @property CI_Upload $upload                    File Uploading Class
 * @property CI_URI $uri                          Parses URIs and determines routing
 * @property CI_User_agent $user_agent            Identifies the platform, browser, robot, or mobile devise of the browsing agent
 * @property CI_Xmlrpc $xmlrpc                    XML-RPC request handler class
 * @property CI_Xmlrpcs $xmlrpcs                  XML-RPC server class
 * @property CI_Zip $zip                          Zip Compression Class
 * @property CI_Javascript $javascript            Javascript Class
 * @property CI_Jquery $jquery                    Jquery Class
 * @property CI_Utf8 $utf8                        Provides support for UTF-8 environments
 * @property CI_Security $security                Security Class, xss, csrf, etc...
 * @property CI_Driver_Library $driver            CodeIgniter Driver Library Class
 * @property CI_Cache $cache                      CodeIgniter Caching Class
 */
class CI_Model
{
}

;
?>