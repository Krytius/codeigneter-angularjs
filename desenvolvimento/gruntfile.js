module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        meta: {
            src: {
                webservice: {
                    raiz: "webservice/",
                    application: "<%= meta.src.webservice.raiz %>application",
                    system: "<%= meta.src.webservice.raiz %>system"
                },
                assets: {
                    raiz: "assets/",
                    img: "<%= meta.src.assets.raiz %>img/",
                    style: "<%= meta.src.assets.raiz %>style/",
                    script: "<%= meta.src.assets.raiz %>script/",
                    font: "<%= meta.src.assets.raiz %>font/"
                },
                view: "views/"
            },
            homologacao: {
                raiz: "../homologacao/<%= pkg.version %>/",
                webservice: "<%= meta.homologacao.raiz %>webservice/",
                assets: {
                    raiz: "<%= meta.homologacao.raiz %>assets/",
                    img: "<%= meta.homologacao.assets.raiz %>img/",
                    script: "<%= meta.homologacao.assets.raiz %>script/",
                    style: "<%= meta.homologacao.assets.raiz %>style/",
                    font: "<%= meta.homologacao.assets.raiz %>font/"
                },
                view: "<%= meta.homologacao.raiz %>views/"
            },
            producao: {
                raiz: "../producao/<%= pkg.version %>/",
                webservice: "<%= meta.producao.raiz %>webservice/",
                assets: "<%= meta.producao.raiz %>assets/",
                view: "<%= meta.producao.raiz %>views/"
            }
        },
        watch: {
            html: {
                files: [
                    'index.tpl.html', '<%= meta.src.assets.script %>lib/*.js', '<%= meta.src.assets.script %>lib/*.css'
                ],
                tasks: ['clean:indexHTML', 'wiredep', 'includeSource:liveReload']
            },
            outros: {
                files: [
                    '<%= meta.src.view %>**/*.html',
                    '<%= meta.src.assets.script %>**/*.js',
                    '<%= meta.src.assets.font %>**',
                    '<%= meta.src.assets.img %>**',
                    '<%= meta.src.assets.style %>*.css'
                ],
                tasks: ['includeSource:liveReload']
            },
            options: {
                livereload: true
            }
        },
        includeSource: {
            options: {
                basePath: '',
                baseUrl: ''
            },
            liveReload: {
                files: {
                    "index.html": "index.tpl.html"
                }
            }
        },
        wiredep: {
            target: {
                src: 'index.tpl.html',
                options: {
                    directory: '<%= meta.src.assets.script %>lib'
                },
                fileTypes: {
                    html: {
                        replace: {
                            js: '<script src="{{filePath}}"></script>',
                            css: '<link rel="stylesheet" href="{{filePath}}" />'
                        }
                    }
                }
            }
        },
        // Union project
        uglify: {
            test: {
                files: [{
                        expand: true,
                        cwd: "<%= meta.src.assets.script %>",
                        src: ['**/*.js', '!lib/**/*'],
                        dest: '<%= meta.homologacao.assets.script %>'
                    }]
            }
        },
        cssmin: {
            test: {
                files: [{
                        expand: true,
                        cwd: "<%= meta.src.assets.style %>",
                        src: '**/*.css',
                        dest: '<%= meta.homologacao.assets.style %>'
                    }]
            }
        },
        compress: {
            build: {
                options: {
                    archive: '../producao/<%= pkg.name %>_<%= pkg.version %>_producao.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= meta.producao.raiz %>',
                        src: ['**', '**/.htaccess', '**/*.html'],
                        dest: '../../producao/'
                    }
                ]
            },
			test: {
                options: {
                    archive: '../homologacao/<%= pkg.name %>_<%= pkg.version %>_homologacao.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= meta.homologacao.raiz %>',
                        src: [
							'**', '**/.htaccess', '**/*.html',
							'!node_modules/**', '!.bowerrc', '!bower.json'
						],
                        dest: '../homologacao/'
                    }
                ]
            }
        },
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: 'webservice',
                        src: ['**', '**/.htaccess'],
                        dest: '<%= meta.producao.webservice %>'
                    },
                    {
                        expand: true,
                        cwd: 'views',
                        src: ['**'],
                        dest: '<%= meta.producao.view %>'
                    },
                    {
                        expand: true,
                        cwd: 'assets',
                        src: ['**'],
                        dest: '<%= meta.producao.assets %>'
                    },
                    {
                        expand: true,
                        cwd: './',
                        src: ['index.html'],
                        dest: '<%= meta.producao.raiz %>'
                    }
                ]
            },
            test: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= meta.src.webservice.raiz %>',
                        src: ['**', '!user_guide/**', '**/.htaccess'],
                        dest: '<%= meta.homologacao.webservice %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= meta.src.view %>',
                        src: ['**'],
                        dest: '<%= meta.homologacao.view %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= meta.src.assets.script %>',
                        src: ['lib/**'],
                        dest: '<%= meta.homologacao.assets.script %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= meta.src.assets.img %>',
                        src: ['**'],
                        dest: '<%= meta.homologacao.assets.img%>'
                    },
                    {
                        expand: true,
                        cwd: '<%= meta.src.assets.font %>',
                        src: ['**'],
                        dest: '<%= meta.homologacao.assets.font %>'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/',
                        src: ['**'],
                        dest: '<%= meta.homologacao.raiz %>/node_modules/'
                    },
                    {
                        expand: true,
                        cwd: './',
                        filter: 'isFile',
                        src: ['gruntfile.js', 'package.json'],
                        dest: '<%= meta.homologacao.raiz %>'
                    },
                    {
                        expand: true,
                        cwd: '<%= meta.src.raiz %>',
                        src: ['index.html'],
                        dest: '<%= meta.homologacao.raiz %>'
                    }
                ]
            }
        },
        clean: {
            indexHTML: ["index.html"],
            build: ["<%= meta.producao.raiz %>"],
            test: ["<%= meta.homologacao.raiz %>"]
        }
    });

    //Comandos
    grunt.registerTask("itens", ['clean:indexHTML', 'wiredep', 'includeSource:liveReload']);
    grunt.registerTask('devel', ['itens', 'watch']);
    grunt.registerTask('test', ['itens', "clean:test", "uglify:test", "cssmin:test", "copy:test", "compress:test"]);
    grunt.registerTask('build', ["clean:build", "copy:build", "compress:build"]);

};