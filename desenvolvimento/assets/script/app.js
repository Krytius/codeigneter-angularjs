var app = angular.module("projeto", ['ui.router']);

app.run(function ($rootScope, $http) {
});

app.config(function ($stateProvider, $urlRouterProvider) {
    var patchView = "views/";
	
    $urlRouterProvider.otherwise("/admin/home");
	
	$stateProvider
	.state('login', {
		url: "/",
		templateUrl: patchView + "login.html",
		controller: "LoginCtrl"
	})
	
	.state('admin', {
		url: "/admin",
		abstract: true,
		templateUrl: patchView + "admin.html",
		controller: "AdminCtrl"
	})
	
	.state('admin.home', {
		url: "/home",
		views: {
			conteudo: {
				templateUrl: patchView + "dashboard/dash.html",
			}
		},
		controller: "DashCtrl"
	});
	
	
	


});