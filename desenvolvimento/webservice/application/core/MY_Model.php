<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *  Classe estilizada de model para as necessidade do projeto
 */
class MY_Model extends CI_Model
{
    private $dbAcao;
    private $dbConsulta;
    public $tabela;
    public $primaryKey;


    function __construct()
    {
        parent::__construct();

        $this->dbAcao = $this->load->database('acao', TRUE);
        $this->dbConsulta = $this->load->database('default', TRUE);
    }

    /**
     * Método com retorno do banco de dados de ação para transactions
     * @return object
     */
    public function getDbAcao()
    {
        return $this->dbAcao;
    }

    /**
     * Método com retorno do banco de dados de ação para transactions
     * @return object
     */
    public function getDbConsulta()
    {
        return $this->dbConsulta;
    }

    /**
     * Método que faz inserção simples de unica linha no banco de dados
     * @param $keyValue
     * @return object
     * @throws Exception    //--- Erros de inserção e seu código
     */
    public function insert($keyValue)
    {
        try {
            $resposta = array();
            $this->dbAcao->insert($this->tabela, $keyValue);

            $id = $this->dbAcao->insert_id();
            $query = $this->dbAcao->last_query();

            if ($this->dbAcao->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['id'] = $id;
            $resposta['query'] = $query;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que faz insert em uma tabela multiplas linhas de uma vez e retorna todo esse montate em uma única resposta.
     * @param $arrayKeyValue
     * @return object
     */
    public function insertMultiple($arrayKeyValue)
    {
        $resposta1 = array();
        $resposta2 = array();

        foreach ($arrayKeyValue as $k => $v) {
            $this->dbAcao->insert($this->tabela, $v);

            $id = $this->dbAcao->insert_id();
            $query = $this->dbAcao->last_query();

            if ($this->dbAcao->_error_number()) {
                $naoConcluido = array(
                    'error' => true,
                    'code' => $this->dbAcao->_error_number(),
                    'msg' => $this->dbAcao->_error_message(),
                    'query' => $query
                );
                array_push($resposta1, $naoConcluido);
            }

            $concluido = array(
                'error' => false,
                'id' => $id,
                'query' => $query
            );
            array_push($resposta2, $concluido);
        }

        return (object)array(
            'success' => $resposta2,
            'fail' => $resposta1
        );
    }

    /**
     * Método de update através do primary key da tabela.
     * @param $keyValue
     * @param $id
     * @return object
     * @throws Exception    //--- Caso não consiga fazer o error_mensagem, error_code
     */
    public function update($keyValue, $id)
    {
        try {

            if (count($keyValue) === 0 || !$id) {
                show_error("Não é possivel fazer update sem os parametros de set e id", 500, "MY_MODEL");
            }

            $this->dbAcao->where($this->primaryKey, $id);
            $this->dbAcao->update($this->tabela, $keyValue);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método de update através de where específico sem primary key envolvida.
     * @param $keyValue
     * @param $where
     * @return object
     * @throws Exception    //--- Caso não consiga fazer update error_mensagem, error_code
     */
    public function updateFind($keyValue, $where)
    {
        try {

            if (count($keyValue) === 0 || count($where) === 0) {
                show_error("Não é possivel fazer update sem os parametros de set e where", 500, "MY_MODEL");
            }

            $this->dbAcao->where($where);
            $this->dbAcao->update($this->tabela, $keyValue);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que faz update de uma tabela inteira sem nenhuma busca. Cuidado ao utilizar este método.
     * @param $keyValue
     * @return object
     * @throws Exception    //--- Caso não consiga fazer update error_mensagem, error_code
     */
    public function updateAll($keyValue)
    {
        try {
            if (count($keyValue) === 0) {
                show_error("Não é possivel fazer update sem os parametros de set", 500, "MY_MODEL");
            }

            $this->dbAcao->update($this->tabela, $keyValue);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que deleta registro pelo id primary key
     * @param $id
     * @return object
     * @throws Exception    //--- Caso não consiga fazer delete error_mensagem, error_code
     */
    public function delete($id)
    {
        try {

            if (!$id) {
                show_error("Não é possivel deletar um registro sem setar seu id.", 500, "MY_MODEL");
            }

            $this->dbAcao->where($this->primaryKey, $id);
            $this->dbAcao->delete($this->tabela);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que apaga registro por um where mais elaborado
     * @param $where
     * @return object
     * @throws Exception    //--- Caso não consiga fazer delete error_mensagem, error_code
     */
    public function deleteFind($where)
    {
        try {

            if (count($where) === 0) {
                show_error("Não é possivel deletar um registro sem setar uma consulta.", 500, "MY_MODEL");
            }

            $this->dbAcao->where($where);
            $this->dbAcao->delete($this->tabela);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que deleta todos os registros da tabela
     * @return object
     * @throws Exception    //--- Caso não consiga fazer delete error_mensagem, error_code
     */
    public function deleteAll()
    {
        try {
            $this->dbAcao->delete($this->tabela);

            $query = $this->dbAcao->last_query();
            $affectedRows = $this->dbAcao->affected_rows();

            if ($this->db->_error_number()) {
                throw new Exception($this->dbAcao->_error_message(), $this->dbAcao->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['affectedRows'] = $affectedRows;
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        return (object)$resposta;
    }

    /**
     * Método que faz a busca na tabela indicada e traz informações em forma de object
     * @param $where
     * @param string $select
     * @param array $orderBy
     * @param array $groupBy
     * @param int $limit
     * @return object
     */
    public function get($where, $select = "", $orderBy = array(), $groupBy = array(), $limit = 0)
    {
        try {

            $this->dbConsulta->select(($select !== "") ? $select : "");
            $this->dbConsulta->from($this->tabela);

            if (count($where) != 0) {
                foreach ($where as $key => $val) {
                    if (gettype($val) !== 'array') {
                        $this->dbConsulta->where($key, $val);
                    } else {
                        if ($val[0] === 'NULL') {
                            $this->dbConsulta->where($key . " " . $val[1] . " NULL ");
                        } else if ($val[0] === 'OR_NULL') {
                            $this->dbConsulta->or_where($key . " " . $val[1] . " NULL ");
                        } else if ($val[0] === 'IN') {
                            $this->dbConsulta->where_in($key, $val[1]);
                        } else if ($val[0] === 'OR') {
                            $this->dbConsulta->or_where($key, $val[1]);
                        } else if ($val[0] === 'LIKE') {
                            $this->dbConsulta->like($key, $val[1], $val[2]);
                        }
                    }
                }
            }

            if (count($orderBy) > 0) {
                $this->dbConsulta->order_by($orderBy);
            }

            if (count($groupBy) > 0) {
                $this->dbConsulta->group_by($groupBy);
            }

            if ($limit > 0) {
                $this->dbConsulta->limit($limit);
            }

            $result = $this->dbConsulta->get();
            $query = $this->dbConsulta->last_query();

            if (!($result->num_rows() > 0)) {
                throw new Exception($this->dbConsulta->_error_message(), $this->dbConsulta->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['data'] = $result->result();
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        $this->dbConsulta->close();
        return (object)$resposta;
    }

    /**
     * Método que faz busca mas só retorna uma unica linha com os parametros solicitados
     * @param $where
     * @param string $select
     * @param array $orderBy
     * @param array $groupBy
     * @param int $limit
     * @return object
     */
    public function getRow($where, $select = "", $orderBy = array(), $groupBy = array(), $limit = 0)
    {
        try {

            if (count($where) === 0) {
                show_error("Não é possivel fazer uma busca sem filtros.", 500, "MY_MODEL");
            }

            $this->dbConsulta->select(($select !== "") ? $select : "");
            $this->dbConsulta->from($this->tabela);

            foreach ($where as $key => $val) {
                if (gettype($val) !== 'array') {
                    $this->dbConsulta->where($key, $val);
                } else {
                    if ($val[0] === 'NULL') {
                        $this->dbConsulta->where($key . " " . $val[1] . " NULL ");
                    } else if ($val[0] === 'OR_NULL') {
                        $this->dbConsulta->or_where($key . " " . $val[1] . " NULL ");
                    } else if ($val[0] === 'IN') {
                        $this->dbConsulta->where_in($key, $val[1]);
                    }
                }
            }

            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $val) {
                    $this->dbConsulta->order_by($key, $val);
                }
            }

            if (count($groupBy) > 0) {
                $this->dbConsulta->group_by($groupBy);
            }

            if ($limit > 0) {
                $this->dbConsulta->limit($limit);
            }

            $result = $this->dbConsulta->get();
            $query = $this->dbConsulta->last_query();

            if ($this->dbConsulta->_error_message()) {
                throw new Exception($this->dbConsulta->_error_message(), $this->dbConsulta->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['numRows'] = $result->num_rows();
            $resposta['data'] = $result->row();
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        $this->dbConsulta->close();
        return (object)$resposta;
    }

    /**
     * Método que traz toda a tabela de acordo com a solicitação de organização
     * @param array $orderBy
     * @param int $limit
     * @return object
     */
    public function getAll($orderBy = array(), $limit = 0)
    {

        try {

            $this->dbConsulta->select();
            $this->dbConsulta->from($this->tabela);

            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $val) {
                    $this->dbConsulta->order_by($key, $val);
                }
            }

            if ($limit > 0) {
                $this->dbConsulta->limit($limit);
            }

            $result = $this->dbConsulta->get();
            $query = $this->dbConsulta->last_query();

            if ($this->dbConsulta->_error_message()) {
                throw new Exception($this->dbConsulta->_error_message(), $this->dbConsulta->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['numRows'] = $result->num_rows();
            $resposta['data'] = $result->result();
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        $this->dbConsulta->close();
        return (object)$resposta;

    }

    public function getId($id, $select = "")
    {
        try {

            $this->dbConsulta->select(($select !== "") ? $select : "");
            $this->dbConsulta->from($this->tabela);
            $this->dbConsulta->where($this->primaryKey, $id);


            $result = $this->dbConsulta->get();
            $query = $this->dbConsulta->last_query();

            if ($this->dbConsulta->_error_message()) {
                throw new Exception($this->dbConsulta->_error_message(), $this->dbConsulta->_error_number());
            }

            $resposta['error'] = false;
            $resposta['query'] = $query;
            $resposta['data'] = $result->row();
        } catch (Exception $e) {
            $resposta['error'] = true;
            $resposta['code'] = $e->getCode();
            $resposta['msg'] = $e->getMessage();
            $resposta['query'] = $query;
        }

        $this->dbConsulta->close();
        return (object)$resposta;
    }
}
